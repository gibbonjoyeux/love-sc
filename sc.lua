--------------------------------------------------------------------------------
--- MODULE sc.lua
--------------------------------------------------------------------------------

local socket	= require( 'socket' )
local ffi		= require( 'ffi' )
local osc		= require( 'osc' )

local sc		= {}

--------------------------------------------------------------------------------
--- LOCAL FUNCTIONS
--------------------------------------------------------------------------------

function		math.log2( x )
	return math.log( x ) / math.log( 2 )
end

--------------------------------------------------------------------------------
--- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------

--------------------------------------------------
--- INIT
--------------------------------------------------

function		sc.init( num_channels, num_hardware_busses, num_groups )
	if num_channels == nil then num_channels = 2 end
	if num_hardware_busses == nil then num_hardware_busses = 4 end
	if num_groups == nil then num_groups = 1024 end
	-- INIT DATA
	sc.num_channels = num_channels
	sc.num_hardware_busses = num_hardware_busses
	sc.num_groups = num_groups
	sc.sc_id = num_groups
	sc.sc_groups = ffi.new( 'char[?]', num_groups )
	sc.sc_busses = ffi.new( 'char[?]', 1020 )
	sc.sc_buffers = ffi.new( 'char[?]', 1024 )
	sc.pwd = os.getenv( 'PWD' ) .. '/'
	sc.HEAD = 0
	sc.TAIL = 1
	sc.BEFORE = 2
	sc.AFTER = 3
	sc.REPLACE = 4
	-- INIT SCALES
	sc.scale_init()
	-- INIT SC
	osc.init( '127.0.0.1', 57110 )
end

--------------------------------------------------
--- SYNTH
--------------------------------------------------

function		sc.load( path )
	local		source
	local		file
	local		data
	local		length
	local		values, value
	local		name
	local		i
	local		err

	source = {}
	source.path = path
	-- OPEN FILE
	file, err = io.open( path, 'r' )
	if file == nil then return nil end
	-- READ FILE
	--- READ HEADER
	---- MAGIC NUMBER
	if file:read( 4 ) ~= 'SCgf' then return nil end
	---- SC DEF VERSION
	data = file:read( 4 )
	if data == nil then return nil end
	if love.data.unpack( '>i4', data ) ~= 2 then return nil end
	---- DEF COUNT
	data = file:read( 2 )
	if data == nil then return nil end
	if love.data.unpack( '>i2', data ) ~= 1 then return nil end
	--- READ DEFINITION
	---- NAME
	data = file:read( 1 )
	value = love.data.unpack( '>I1', data )
	source.name = file:read( value )
	---- CONSTANTS
	data = file:read( 4 )
	value = love.data.unpack( '>i4', data )
	file:read( 4 * value )
	---- PARAMETERS VALUES
	values = {}
	data = file:read( 4 )
	length = love.data.unpack( '>i4', data )
	for i = 1, length do
		data = file:read( 4 )
		value = love.data.unpack( '>f', data )
		values[ i - 1 ] = value
	end
	---- PARAMETERS NAMES
	source.params = {}
	source.indexes = {}
	data = file:read( 4 )
	length = love.data.unpack( '>i4', data )
	for i = 1, length do
		-- GET NAME
		data = file:read( 1 )
		value = love.data.unpack( '>I1', data )
		name = file:read( value )
		-- GET INDEX
		data = file:read( 4 )
		value = love.data.unpack( '>i4', data )
		-- INSERT
		source.params[ name ] = values[ value ]
		source.indexes[ name ] = value
		print( name, value )
	end
	-- LOAD INTO SC
	osc.call_start( 'd_load' )
	osc.call_string( sc.pwd .. path )
	osc.call_make()
	-- RETURN SOURCE
	return source
end

function		sc.play( source, args, action, target )
	osc.call_start( 's_new' )
	osc.call_string( source.name )
	osc.call_int( sc.sc_id )
	osc.call_int( action or 0 )
	osc.call_int( target or 0 )
	for key in pairs( args ) do
		osc.call_int( source.indexes[ key ] )
		osc.call_float( args[ key ] )
	end
	osc.call_make()
	sc.sc_id = sc.sc_id + 1
	return sc.sc_id - 1
end

function		sc.set( source, voice, args )
	osc.call_start( 'n_set' )
	osc.call_int( voice )
	for key in pairs( args ) do
		osc.call_int( source.indexes[ key ] )
		osc.call_float( args[ key ] )
	end
	osc.call_make()
end

function		sc.stop( source, voice )
	if source.indexes.gate == nil then
		return
	end
	osc.call_start( 'n_set' )
	osc.call_int( voice )
	osc.call_int( source.indexes.gate )
	osc.call_float( 0 )
	osc.call_make()
end

--------------------------------------------------
--- NODES
--------------------------------------------------

function		sc.kill( node )
	osc.call_start( 'n_free' )
	osc.call_int( node )
	osc.call_make()
end

function		sc.before( node_1, node_2 )
	osc.call_start( 'n_before' )
	osc.call_int( node_1 )
	osc.call_int( node_2 )
	osc.call_make()
end

function		sc.after( node_1, node_2 )
	osc.call_start( 'n_after' )
	osc.call_int( node_1 )
	osc.call_int( node_2 )
	osc.call_make()
end

function		sc.head( group, node )
	osc.call_start( 'g_head ' )
	osc.call_int( group )
	osc.call_int( node )
	osc.call_make()
end

function		sc.tail( group, node )
	osc.call_start( 'g_tail ' )
	osc.call_int( group )
	osc.call_int( node )
	osc.call_make()
end

function		sc.order( action, target, nodes )
	osc.call_start( 'n_order' )
	osc.call_int( action )
	osc.call_int( target )
	for _, node in ipairs( nodes ) do
		osc.call_int( node )
	end
	osc.call_make()
end

function		sc.free_all()
	osc.call_start( 'g_freeAll' )
	osc.call_int( 0 )
	osc.call_make()
end

--------------------------------------------------
--- GROUPS
--------------------------------------------------

function		sc.group_new( action, target )
	local		id

	-- MAKE GROUP
	i = nil
	for i = 0, 1023 do
		if sc.sc_groups[ i ] == 0 then
			sc.sc_groups[ i ] = 1
			id = i
		end
	end
	if id == nil then
		return nil
	end
	-- MAKE SC GROUP
	osc.call_start( 'g_new' )
	osc.call_int( id )
	osc.call_int( action or 0 )
	osc.call_int( target or 0 )
	osc.call_make()
	-- RETURN GROUP ID
	return id
end

function		sc.group_free( node )
	-- FREE GROUP
	sc.sc_groups[ node ] = 0
	-- FREE SC GROUP
	osc.call_start( 'n_free' )
	osc.call_int( node )
	osc.call_make()
end

--------------------------------------------------
--- BUSSES
--------------------------------------------------

function		sc.bus_new()
	local		id

	i = nil
	for i = 0, 1019, sc.num_channels do
		if sc.sc_busses[ i ] == 0 then
			sc.sc_busses[ i ] = 1
			id = i
		end
	end
	return sc.num_hardware_busses + id
end

function		sc.bus_free( bus )
	sc.sc_busses[ bus - sc.num_hardware_busses ] = 0
end

--------------------------------------------------
--- BUFFERS
--------------------------------------------------

--------------------------------------------------
--- TOOLS
--------------------------------------------------

function		sc.midi_ratio( midi )
	return 2.0 ^ ( midi / 12.0 )
end

function		sc.ratio_midi( ratio )
	return 12.0 * math.log2( ratio )
end

function		sc.midi_cps( midi, base )
	if base == nil then
		base = 440
	end
	return base * ( 2.0 ^ ( ( midi - 69 ) / 12.0 ) )
end

function		sc.cps_midi( cps, base )
	if base == nil then
		base = 440
	end
	return 12.0 * math.log2( cps / base ) + 69
end

function		sc.exprand( a, b )
	if a == nil then
		return math.log10( 1.0 + math.random() * 9.0 )
	elseif b == nil then
		return math.log10( 1.0 + math.random() * 9.0 ) * a
	else
		return a + math.log10( 1.0 + math.random() * 9.0 ) * ( b - a )
	end
end

------------------------------
--- SCALES
------------------------------

function		sc.scale_new( scale, base )
	for i in pairs( scale ) do
		scale[ i ] = ( scale[ i ] / base ) * 12.0
	end
	return scale
end

function		sc.scale_init()
	sc.scale = {}
	-- TWELVE TONES PER OCTAVE
	-- 5 note scales
	sc.scale.minorPentatonic = sc.scale_new( { 0,3,5,7,10 }, 12 )
	sc.scale.majorPentatonic = sc.scale_new( { 0,2,4,7,9 }, 12 )
	-- another mode of major pentatonic
	sc.scale.ritusen = sc.scale_new( { 0,2,5,7,9 }, 12 )
	-- another mode of major pentatonic
	sc.scale.egyptian = sc.scale_new( { 0,2,5,7,10 }, 12 )
	sc.scale.kumoi = sc.scale_new( { 0,2,3,7,9 }, 12 )
	sc.scale.hirajoshi = sc.scale_new( { 0,2,3,7,8 }, 12 )
	sc.scale.iwato = sc.scale_new( { 0,1,5,6,10 }, 12 )
	sc.scale.chinese = sc.scale_new( { 0,4,6,7,11 }, 12 )
	sc.scale.indian = sc.scale_new( { 0,4,5,7,10 }, 12 )
	sc.scale.pelog = sc.scale_new( { 0,1,3,7,8 }, 12 )
	sc.scale.prometheus = sc.scale_new( { 0,2,4,6,11 }, 12 )
	sc.scale.scriabin = sc.scale_new( { 0,1,4,7,9 }, 12 )
	-- han chinese pentatonic scales
	sc.scale.gong = sc.scale_new( { 0,2,4,7,9 }, 12 )
	sc.scale.shang = sc.scale_new( { 0,2,5,7,10 }, 12 )
	sc.scale.jiao = sc.scale_new( { 0,3,5,8,10 }, 12 )
	sc.scale.zhi = sc.scale_new( { 0,2,5,7,9 }, 12 )
	sc.scale.yu = sc.scale_new( { 0,3,5,7,10 }, 12 )
	-- 6 note scales
	sc.scale.whole = sc.scale_new( { 0,2,4,8,10 }, 12 )
	sc.scale.augmented = sc.scale_new( { 0,3,4,7,8,11 }, 12 )
	sc.scale.augmented2 = sc.scale_new( { 0,1,4,5,8,9 }, 12 )
	-- Partch's Otonalities and Utonalities
	--sc.scale.partch_o1 = sc.scale_new( { 0,8,14,20,25,34 }, 43,
	--Tuning.partch, "Partch Otonality 1"),
	--sc.scale.partch_o2 = sc.scale_new( { 0,7,13,18,27,35 }, 43,
	--Tuning.partch, "Partch Otonality 2"),
	--sc.scale.partch_o3 = sc.scale_new( { 0,6,12,21,29,36 }, 43,
	--Tuning.partch, "Partch Otonality 3"),
	--sc.scale.partch_o4 = sc.scale_new( { 0,5,15,23,30,37 }, 43,
	--Tuning.partch, "Partch Otonality 4"),
	--sc.scale.partch_o5 = sc.scale_new( { 0,10,18,25,31,38 }, 43,
	--Tuning.partch, "Partch Otonality 5"),
	--sc.scale.partch_o6 = sc.scale_new( { 0,9,16,22,28,33 }, 43,
	--Tuning.partch, "Partch Otonality 6"),
	--sc.scale.partch_u1 = sc.scale_new( { 0,9,18,23,29,35 }, 43,
	--Tuning.partch, "Partch Utonality 1"),
	--sc.scale.partch_u2 = sc.scale_new( { 0,8,16,25,30,36 }, 43,
	--Tuning.partch, "Partch Utonality 2"),
	--sc.scale.partch_u3 = sc.scale_new( { 0,7,14,22,31,37 }, 43,
	--Tuning.partch, "Partch Utonality 3"),
	--sc.scale.partch_u4 = sc.scale_new( { 0,6,13,20,28,38 }, 43,
	--Tuning.partch, "Partch Utonality 4"),
	--sc.scale.partch_u5 = sc.scale_new( { 0,5,12,18,25,33 }, 43,
	--Tuning.partch, "Partch Utonality 5"),
	--sc.scale.partch_u6 = sc.scale_new( { 0,10,15,21,27,34 }, 43,
	--Tuning.partch, "Partch Utonality 6"),
	-- hexatonic modes with no tritone
	sc.scale.hexMajor7 = sc.scale_new( { 0,2,4,7,9,11 }, 12 )
	sc.scale.hexDorian = sc.scale_new( { 0,2,3,5,7,10 }, 12 )
	sc.scale.hexPhrygian = sc.scale_new( { 0,1,3,5,8,10 }, 12 )
	sc.scale.hexSus = sc.scale_new( { 0,2,5,7,9,10 }, 12 )
	sc.scale.hexMajor6 = sc.scale_new( { 0,2,4,5,7,9 }, 12 )
	sc.scale.hexAeolian = sc.scale_new( { 0,3,5,7,8,10 }, 12 )
	-- 7 note scales
	sc.scale.major = sc.scale_new( { 0,2,4,5,7,9,11 }, 12 )
	sc.scale.ionian = sc.scale_new( { 0,2,4,5,7,9,11 }, 12 )
	sc.scale.dorian = sc.scale_new( { 0,2,3,5,7,9,10 }, 12 )
	sc.scale.phrygian = sc.scale_new( { 0,1,3,5,7,8,10 }, 12 )
	sc.scale.lydian = sc.scale_new( { 0,2,4,6,7,9,11 }, 12 )
	sc.scale.mixolydian = sc.scale_new( { 0,2,4,5,7,9,10 }, 12 )
	sc.scale.aeolian = sc.scale_new( { 0,2,3,5,7,8,10 }, 12 )
	sc.scale.minor = sc.scale_new( { 0,2,3,5,7,8,10 }, 12 )
	sc.scale.locrian = sc.scale_new( { 0,1,3,5,6,8,10 }, 12 )
	sc.scale.harmonicMinor = sc.scale_new( { 0,2,3,5,7,8,11 }, 12 )
	sc.scale.harmonicMajor = sc.scale_new( { 0,2,4,5,7,8,11 }, 12 )
	sc.scale.melodicMinor = sc.scale_new( { 0,2,3,5,7,9,11 }, 12 )
	sc.scale.melodicMinorDesc = sc.scale_new( { 0,2,3,5,7,8,10 }, 12 )
	sc.scale.melodicMajor = sc.scale_new( { 0,2,4,5,7,8,10 }, 12 )
	sc.scale.bartok = sc.scale_new( { 0,2,4,5,7,8,10 }, 12 )
	sc.scale.hindu = sc.scale_new( { 0,2,4,5,7,8,10 }, 12 )
	-- raga modes
	sc.scale.todi = sc.scale_new( { 0,1,3,6,7,8,11 }, 12 )
	sc.scale.purvi = sc.scale_new( { 0,1,4,6,7,8,11 }, 12 )
	sc.scale.marva = sc.scale_new( { 0,1,4,6,7,9,11 }, 12 )
	sc.scale.bhairav = sc.scale_new( { 0,1,4,5,7,8,11 }, 12 )
	sc.scale.ahirbhairav = sc.scale_new( { 0,1,4,5,7,9,10 }, 12 )
	sc.scale.superLocrian = sc.scale_new( { 0,1,3,4,6,8,10 }, 12 )
	sc.scale.romanianMinor = sc.scale_new( { 0,2,3,6,7,9,10 }, 12 )
	sc.scale.hungarianMinor = sc.scale_new( { 0,2,3,6,7,8,11 }, 12 )
	sc.scale.neapolitanMinor = sc.scale_new( { 0,1,3,5,7,8,11 }, 12 )
	sc.scale.enigmatic = sc.scale_new( { 0,1,4,6,8,10,11 }, 12 )
	sc.scale.spanish = sc.scale_new( { 0,1,4,5,7,8,10 }, 12 )
	-- modes of whole tones with added note ->
	sc.scale.leadingWhole = sc.scale_new( { 0,2,4,6,8,10,11 }, 12 )
	sc.scale.lydianMinor = sc.scale_new( { 0,2,4,6,7,8,10 }, 12 )
	sc.scale.neapolitanMajor = sc.scale_new( { 0,1,3,5,7,9,11 }, 12 )
	sc.scale.locrianMajor = sc.scale_new( { 0,2,4,5,6,8,10 }, 12 )
	-- 8 note scales
	sc.scale.diminished = sc.scale_new( { 0,1,3,4,6,7,9,10 }, 12 )
	sc.scale.diminished2 = sc.scale_new( { 0,2,3,5,6,8,9,11 }, 12 )
	-- 12 note scales
	sc.scale.chromatic = sc.scale_new( { 0,1,2,3,4,5,6,7,8,9,10,11 }, 12 )
	-- TWENTY-FOUR TONES PER OCTAVE
	sc.scale.chromatic24 = sc.scale_new( { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,23 }, 24 )
	-- maqam ajam
	sc.scale.ajam = sc.scale_new( { 0,4,8,10,14,18,22 }, 24 )
	sc.scale.jiharkah = sc.scale_new( { 0,4,8,10,14,18,21 }, 24 )
	sc.scale.shawqAfza = sc.scale_new( { 0,4,8,10,14,16,22 }, 24 )
	-- maqam sikah
	sc.scale.sikah = sc.scale_new( { 0,3,7,11,14,17,21 }, 24 )
	sc.scale.sikahDesc = sc.scale_new( { 0,3,7,11,13,17,21 }, 24 )
	sc.scale.huzam = sc.scale_new( { 0,3,7,9,15,17,21 }, 24 )
	sc.scale.iraq = sc.scale_new( { 0,3,7,10,13,17,21 }, 24 )
	sc.scale.bastanikar = sc.scale_new( { 0,3,7,10,13,15,21 }, 24 )
	sc.scale.mustar = sc.scale_new( { 0,5,7,11,13,17,21 }, 24 )
	-- maqam bayati
	sc.scale.bayati = sc.scale_new( { 0,3,6,10,14,16,20 }, 24 )
	sc.scale.karjighar = sc.scale_new( { 0,3,6,10,12,18,20 }, 24 )
	sc.scale.husseini = sc.scale_new( { 0,3,6,10,14,17,21 }, 24 )
	-- maqam nahawand
	sc.scale.nahawand = sc.scale_new( { 0,4,6,10,14,16,22 }, 24 )
	sc.scale.nahawandDesc = sc.scale_new( { 0,4,6,10,14,16,20 }, 24 )
	sc.scale.farahfaza = sc.scale_new( { 0,4,6,10,14,16,20 }, 24 )
	sc.scale.murassah = sc.scale_new( { 0,4,6,10,12,18,20 }, 24 )
	sc.scale.ushaqMashri = sc.scale_new( { 0,4,6,10,14,17,21 }, 24 )
	-- maqam rast
	sc.scale.rast = sc.scale_new( { 0,4,7,10,14,18,21 }, 24 )
	sc.scale.rastDesc = sc.scale_new( { 0,4,7,10,14,18,20 }, 24 )
	sc.scale.suznak = sc.scale_new( { 0,4,7,10,14,16,22 }, 24 )
	sc.scale.nairuz = sc.scale_new( { 0,4,7,10,14,17,20 }, 24 )
	sc.scale.yakah = sc.scale_new( { 0,4,7,10,14,18,21 }, 24 )
	sc.scale.yakahDesc = sc.scale_new( { 0,4,7,10,14,18,20 }, 24 )
	sc.scale.mahur = sc.scale_new( { 0,4,7,10,14,18,22 }, 24 )
	-- maqam hijaz
	sc.scale.hijaz = sc.scale_new( { 0,2,8,10,14,17,20 }, 24 )
	sc.scale.hijazDesc = sc.scale_new( { 0,2,8,10,14,16,20 }, 24 )
	sc.scale.zanjaran = sc.scale_new( { 0,2,8,10,14,18,20 }, 24 )
	-- maqam hijazKar
	sc.scale.hijazKar = sc.scale_new( { 0,2,8,10,14,16,22 }, 24 )
	-- maqam saba
	sc.scale.saba = sc.scale_new( { 0,3,6,8,12,16,20 }, 24 )
	sc.scale.zamzam = sc.scale_new( { 0,2,6,8,14,16,20 }, 24 )
	-- maqam kurd
	sc.scale.kurd = sc.scale_new( { 0,2,6,10,14,16,20 }, 24 )
	sc.scale.kijazKarKurd = sc.scale_new( { 0,2,8,10,14,16,22 }, 24 )
	-- maqam nawa Athar
	sc.scale.nawaAthar = sc.scale_new( { 0,4,6,12,14,16,22 }, 24 )
	sc.scale.nikriz = sc.scale_new( { 0,4,6,12,14,18,20 }, 24 )
	sc.scale.atharKurd = sc.scale_new( { 0,2,6,12,14,16,22 }, 24 )
end

--------------------------------------------------------------------------------
--- RETURN MODULE
--------------------------------------------------------------------------------

return sc
