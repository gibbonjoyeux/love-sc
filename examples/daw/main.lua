
local sc		= require( 'sc' )
local slab		= require( 'lib.slab.API' )

function		love.load()
	slab.Initialize()

	-- INIT SC
	sc.init()
	-- LOAD SOURCE
	source = sc.load( './def/sine.scsyndef' )
	love.timer.sleep( 0.5 )
end

function		love.update( dt )
	slab.Update( dt )

	slab.BeginWindow('test', {
		AllowMove = false,
		AllowResize = false,
		AutoSizeWindow = false,
		ResetLayout = true,
		X = 0, Y = 0, W = 100, H = 100,
		Title = 'test',
		ShowMinimize = false })
		slab.Text("Hello World, how are you doing today ?")
		slab.Text("Hello World")
		slab.Text("Hello World")
		slab.Text("Hello World")
	slab.EndWindow()
end

function		love.draw()
	slab.Draw()
end

