
local sc		= require( 'sc' )
local time
local scale
local voice
local source
local delay

function		love.load()
	-- INIT SC
	sc.init()
	-- LOAD SOURCE
	source = sc.load( './def/sine.scsyndef' )
	love.timer.sleep( 0.5 )
	-- INIT MUSIC
	scale = sc.scale.chinese
	time = 0
	delay = 0
end

function		love.update( dt )
	local		chance
	local		midi
	local		amp

	-- PLAY
	time = time + dt
	if time > delay then
		-- KILL PREVIOUS VOICE
		if voice ~= nil then
			sc.stop( source, voice )
		end
		-- RUN NEW VOICE
		midi = 45 + scale[ love.math.random( #scale ) ]
		amp = 0.25
		chance = love.math.random()
		if chance > 0.95 then
			midi = midi + 24
			amp = amp * 0.3
		elseif chance > 0.7 then
			midi = midi + 12
			amp = amp * 0.5
		end
		voice = sc.play( source, {
			freq = sc.midi_cps( midi ),
			atk = 0.0,
			rel = love.math.random( 0.5, 1 ),
			amp = amp
		} )
		-- UPDATE TIME
		time = 0
		delay = ( chance > 0.7 ) and 0.5 or 0.25
	end
	-- QUIT LOVE
	if love.keyboard.isDown( 'escape' ) then
		-- KILL PREVIOUS VOICE
		if voice ~= nil then
			sc.stop( source, voice )
		end
		-- QUIT
		love.event.quit()
	end

end

function		love.draw()
end

