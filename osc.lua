--------------------------------------------------------------------------------
--- MODULE sc.lua
--------------------------------------------------------------------------------

local socket = require( 'socket' )
local ffi = require( 'ffi' )
local osc = {}

--------------------------------------------------------------------------------
--- LOCAL FUNCTIONS
--------------------------------------------------------------------------------

local function	string_close( buffer, i )
	buffer[ i ] = 0
	i = i + 1
	while i % 4 ~= 0 do
		buffer[ i ] = 0
		i = i + 1
	end
	return i
end

--------------------------------------------------------------------------------
--- PUBLIC FUNCTIONS
--------------------------------------------------------------------------------

function		osc.init( address, port )
	ffi.cdef( [[
		typedef		struct {
			char	data[1024];
		} buffer;

		typedef		union {
			int32_t	i32;
			float	f32;
			char	byte[4];
		} endian_conversion;

		typedef		union {
			char	c[1024];
			int32_t	i32[256];
			float	f32[256];
		} call_buffer;

		int		printf(const char *fmt, ...);
	]] )

	-- INIT DATA
	osc.buffer_header = ffi.new( 'char[?]', 1024 )
	osc.buffer_content = ffi.new( 'char[?]', 1024 )
	osc.buffer_header_i = 0
	osc.buffer_content_i = 0
	osc.endian_reverse = ffi.abi( 'le' )
	osc.endian_conv = ffi.new( 'endian_conversion' )
	-- CONNECT TO SUPERCOLLIDER
	osc.udp = socket.udp()
	osc.udp:settimeout( 0 )
	osc.udp:setpeername( address, port )
end

function		osc.call_start( path )
	local		length
	local		i

	-- ADD PATH
	osc.buffer_header[ 0 ] = string.byte( '/' )
	length = #path
	for i = 1, length do
		osc.buffer_header[ i ]  = string.byte( path, i )
	end
	osc.buffer_header_i = string_close( osc.buffer_header, length + 1 )
	osc.buffer_header[ osc.buffer_header_i ] = string.byte( ',' )
	osc.buffer_header_i = osc.buffer_header_i + 1
	-- CLEAR CONTENT
	osc.buffer_content_i = 0
end

function		osc.call_int( value )
	-- ADD TO HEADER
	osc.buffer_header[ osc.buffer_header_i ] = string.byte( 'i' )
	osc.buffer_header_i = osc.buffer_header_i + 1
	-- ADD TO CONTENT
	osc.endian_conv.i32 = value
	if osc.endian_reverse == true then
		osc.endian_conv.byte[0], osc.endian_conv.byte[3]
		--[[--]] = osc.endian_conv.byte[3], osc.endian_conv.byte[0]
		osc.endian_conv.byte[1], osc.endian_conv.byte[2]
		--[[--]] = osc.endian_conv.byte[2], osc.endian_conv.byte[1]
	end
	osc.buffer_content[ osc.buffer_content_i ] = osc.endian_conv.byte[ 0 ]
	osc.buffer_content[ osc.buffer_content_i + 1 ] = osc.endian_conv.byte[ 1 ]
	osc.buffer_content[ osc.buffer_content_i + 2 ] = osc.endian_conv.byte[ 2 ]
	osc.buffer_content[ osc.buffer_content_i + 3 ] = osc.endian_conv.byte[ 3 ]
	osc.buffer_content_i = osc.buffer_content_i + 4
end

function		osc.call_float( value )
	-- ADD TO HEADER
	osc.buffer_header[ osc.buffer_header_i ] = string.byte( 'f' )
	osc.buffer_header_i = osc.buffer_header_i + 1
	-- ADD TO CONTENT
	osc.endian_conv.f32 = value
	if osc.endian_reverse == true then
		osc.endian_conv.byte[0], osc.endian_conv.byte[3]
		--[[--]] = osc.endian_conv.byte[3], osc.endian_conv.byte[0]
		osc.endian_conv.byte[1], osc.endian_conv.byte[2]
		--[[--]] = osc.endian_conv.byte[2], osc.endian_conv.byte[1]
	end
	osc.buffer_content[ osc.buffer_content_i ] = osc.endian_conv.byte[ 0 ]
	osc.buffer_content[ osc.buffer_content_i + 1 ] = osc.endian_conv.byte[ 1 ]
	osc.buffer_content[ osc.buffer_content_i + 2 ] = osc.endian_conv.byte[ 2 ]
	osc.buffer_content[ osc.buffer_content_i + 3 ] = osc.endian_conv.byte[ 3 ]
	osc.buffer_content_i = osc.buffer_content_i + 4
end

function		osc.call_string( value )
	local		length

	-- ADD TO HEADER
	osc.buffer_header[ osc.buffer_header_i ] = string.byte( 's' )
	osc.buffer_header_i = osc.buffer_header_i + 1
	-- ADD TO CONTENT
	length = #value
	for i = 0, length - 1 do
		osc.buffer_content[ osc.buffer_content_i + i ]
		--[[--]] = string.byte( value, i + 1 )
	end
	osc.buffer_content_i = string_close( osc.buffer_content,
	--[[--]] osc.buffer_content_i + length )
end

function		osc.call_make()
	osc.buffer_header_i = string_close( osc.buffer_header, osc.buffer_header_i )
	for i = 0, osc.buffer_content_i - 1 do
		osc.buffer_header[ osc.buffer_header_i + i ]
		--[[--]] = osc.buffer_content[ i ]
	end
	osc.buffer_header_i = osc.buffer_header_i + osc.buffer_content_i
	osc.udp:send( ffi.string( osc.buffer_header, osc.buffer_header_i ) )
end

function		osc.debug()
	print( '[' .. ffi.string( osc.buffer_header, osc.buffer_header_i ) .. ']' )
	print( osc.buffer_header_i )
	print( '[' .. ffi.string( osc.buffer_content, osc.buffer_content_i ) .. ']' )
	print( osc.buffer_content_i )
end

--------------------------------------------------------------------------------
--- RETURN MODULE
--------------------------------------------------------------------------------

return osc
